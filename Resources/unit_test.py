import os

from modules.jamfAPI import validate_mobileconfig_xml


def test_validate_mobileconfig():
    """
    Check Mobile Config profile xml for generic errors

    :return:
    Return True or False based on success
    """

    branch = os.environ.get("BRANCH_NAME")
    # Get instance based on branch found in GCP run environment
    if branch == 'dev':
        instance = "SUTesting"
    elif branch == 'prod':
        instance = "Production"
    else:
        instance = "unknown"

    # Generate run info dictionary from established variables
    run_info = {
        'branch': branch,
        'instance': instance
    }
    valid = validate_mobileconfig_xml("./MobileConfig_Profiles/Nudge.mobileconfig", run_info)
    return valid


test_validate_mobileconfig()
