# ################################################################################
# Author: Samuel Zamvil, Systems Engineer at Stanford University
# Date: 09/27/22
# ################################################################################
import os
from modules.jamfAPI import *

# Retrieve Environment values passed from CloudBuild
username = os.environ.get('USERNAME')
password = os.environ.get('PASSWORD')
uri = os.environ.get('URI')
commit_message = os.environ.get('MESSAGE')
commit_sha = os.environ.get('COMMIT_SHA')
branch = os.environ.get("BRANCH_NAME")

# Instantiate Jamf bearer token
auth = jamfAuth(uri, username, password)

# Determine Jamf instance names based on branch
if branch == 'dev':
    instance = "Development"  # ## Change to Name of Choosing and on Line 228 of modules/jamfAPI.py ## #
elif branch == 'prod':
    instance = "Production"   # ## Change to Name of Choosing and on Line 233 of modules/jamfAPI.py ## #
else:
    instance = "unknown"

# Create dictionary from established variables
run_info = {
    'branch': branch,
    'instance': instance,
    'commit_message': commit_message,
    'commit_sha': commit_sha
}

# Call main update mobile config function
update_osx_mobileconfig("<Your Nudge Configuration Profile Name>", auth,
                        "MobileConfig_Profiles/com.github.macadmins.Nudge.mobileconfig", run_info)
