# ################################################################################
# Author: Samuel Zamvil, Systems Engineer at Stanford University
# Date: 09/27/22
# ################################################################################
import requests
from datetime import datetime
import sys
from html import escape
import base64
import xml.etree.ElementTree
import urllib.parse as parseurl


class jamfAuth:
    def __init__(self, *args):
        """
        The JamfAuth class taks 2 or 3 arguments and expects them each to be in a specific format.
        Instantiating this class will automatically create a bearer token and HTTP headers used for subsequent API calls
        :param args:
        2 arguments present Preencodede Authentication String
        Arg0 is the Jamf URL
        Arg1 is basic auth base64 encoded string, I.e. "username:password"

        3 Arguments present
        Arg0 is the Jamf URL
        Arg1 is the username string
        Arg2 is the password string
        """
        if len(args) == 2:
            if type(args[0]) != str or not args[0].startswith('https://'):
                raise jamfAuth_InvalidArguments('Invalid URI detected')
            else:
                self.URI = args[0]
            try:
                assert base64.decodebytes(args[1].encode())
            except AssertionError:
                raise jamfAuth_InvalidArguments('Credentials provided are not a base64 encoded string.')
            self.__set_token(args[1])
        elif len(args) == 3:
            if type(args[0]) != str or not args[0].startswith('https://'):
                raise jamfAuth_InvalidArguments('Invalid URI detected')
            else:
                self.URI = args[0]
            if type(args[1]) != str or type(args[2]) != str:
                raise jamfAuth_TypeError(f'Invalid type provided for username or password')
            else:
                self.__username = args[1]
                self.__password = args[2]
                self.__set_token(self.__get_basic_auth_encoding())
        else:
            raise jamfAuth_InvalidArguments("An invalid amount of arguments were provided.")
        self.__set_headers()

    def __set_token(self, key):
        """
        Private Setter method which calls the Jamf API to return a bearer token.
        Sets Private class variables of Token and expiry.

        :param key:
        Base64 encoded value for username:password
        """
        url = f"{self.URI}/uapi/auth/tokens"
        headers = {
            f'Authorization': f"Basic {key}"
        }
        response = requests.request("POST", url, headers=headers)
        if response.status_code != 200:
            raise jamfAPI_ResponseError(f'Error creating bearer token. {response.content}')
        self.__BearerToken = response.json()['token']
        self.__TokenExpiry = datetime.fromtimestamp(response.json()['expires'] // 1000)

    def __refresh_token(self):
        """
        Private Setter method which calls Jamf API to refresh token.
        """
        url = f"{self.URI}/uapi/auth/keepAlive"
        response = requests.request("POST", url, headers=self.__Headers)
        if response.status_code != 200:
            self.print_non_fatal_error(f'Error refreshing bearer token. {response.content}')
        else:
            self.__TokenExpiry = datetime.fromtimestamp(int(response.json()['expires'] / 1000))

    def __set_headers(self):
        """
        Private Setter method which sets, URL headers
        """
        self.__Headers = {
            'Accept': 'application/json',
            'Authorization': f'Bearer {self.__BearerToken}'
        }

    def __get_basic_auth_encoding(self):
        """
        Private Getter method which encodes basic authentication username and password.
        """
        encoded_cred_string = f"{self.__username}:{self.__password}".encode('utf-8')
        return base64.b64encode(encoded_cred_string).decode('utf-8')

    def get_headers(self):
        """
        Getter method, returns private headers for HTTP requests
        """
        return self.__Headers

    def check_update_refresh_token(self):
        """
        Call method to check if a refresh token is needed, and refresh if expiry is below 120 seconds.
        For use on long operations.
        """
        if (datetime.now() - self.__TokenExpiry).seconds < 120:
            self.__refresh_token()

    def get_token_expiry(self):
        """
        Getter method with returns token expiry.
        """
        return self.__TokenExpiry

    @staticmethod
    def print_non_fatal_error(message):
        """
        Takes param and prints to stdout.

        :param message:
        """
        print(message, file=sys.stderr)


def build_osx_mobileconfig_payload(_mobileconfig, _redeploy='Newly Assigned'):
    """
    Takes _mobileconfig param which must be HTML encoded and adds to xml payload.
    Takes _redeploy param and adds to xml payload.

    Returns Mobile Config payload for updating via Jamf API
    :param _mobileconfig:
    HTML Encoded mobileConfig file as string
    :param _redeploy:
    This is set to 'Newly Assigned' by default to prevent unwanted
    changes durring testing. Set this to 'All' to redeploy Config Profile to all devices.
    """
    if _redeploy != 'Newly Assigned' or _redeploy != 'All':
        raise jamfAPI_InvalidArguments('Invalid _redeploy argument provided in mobile config payload. '
                                       'Accepted strings: "Newly Assigned", "All"')
    print("Building mobileconfig profile http payload.")
    payload = f'''<?xml version="1.0" encoding="UTF-8"?>
    <os_x_configuration_profile>
        <general>
            <redeploy_on_update>{_redeploy}</redeploy_on_update>
            <payloads>{_mobileconfig}</payloads>
        </general>
    </os_x_configuration_profile>'''
    return payload


def encode_configname(_configname):
    """
    Encode config name for passing in URL to Jamf API

    :param _configname:
    Configuration name exactly as it appears in Jamf.
    """
    print("Encoding config name for URL in http request.")
    _configname_encoded = parseurl.quote(_configname)
    return _configname_encoded


def read_in_mobileconfig(_mobileconfig_file):
    """
    Read in mobileConfig file and return it as a string

    :param _mobileconfig_file:
    Relitive path to mobile config file
    """
    print(f"Reading in {_mobileconfig_file}.")
    mobile_config = ''
    with open(_mobileconfig_file) as open_file:
        for line in open_file.readlines():
            mobile_config += line
    return mobile_config


def put_mobileconfig_update(_payload, _configname, _auth):
    """
    Consume jamf api updating configuration profile

    :param _payload:
    HTML Encoded payload data
    :param _configname:
    URL Encoded config name
    :param _auth:
    jamfAuth object
    :return:
    """
    print(f'Sending config update http request to {_auth.URI}.')
    headers = _auth.get_headers()
    response = requests.put(f"{_auth.URI}/JSSResource/osxconfigurationprofiles/name/{_configname}",
                            data=_payload, headers=headers)
    return response


def notify_slack(message, debug):
    """
    Consumes Slack webhook with message param used in payload.
    Debug param establishes which URI to consume.

    :param message:
    :param debug:
    :return:
    """
    print("Sending Slack Webhook.")
    # ## Add Webhook URL From Slack ## #
    if debug:
        uri = 'https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX' # ## Add Webhook URL From Slack ## #
    else:
        uri = 'https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX' # ## Add Webhook URL From Slack ## #
    headers = {'Content-type': 'application/json'}
    payload = f'{{"text": "{message}", "type": "mrkdwn"}}'
    response = requests.post(headers=headers, data=payload, url=uri)
    print(response.status_code, response.raw)


def notify_slack_success(_run_info):
    """
    Formats sucessful mobileConfig update message to slack and calls notify_slack.

    :param _run_info:
    :return:
    """
    _commit_message = _run_info['commit_message']
    print(f"Successfully updated Jamf Config Profile on {_run_info['instance']}.")
    

    if _run_info['instance'] == 'Development': # ## Mirror Name on Line 16 of DeployNudge.py ## #
        message = f":warning: Pre-Pruduction Nudge Updated :warning:\nThe Nudge configuration has been updated on" \
                  f" {_run_info['instance']}. Testers enrolled in {_run_info['instance']} will begin being " \
                  f"prompted to update to the latest versions of macOS."
        debug = True
    elif _run_info['instance'] == 'Production':  # ## Mirror Name on Line 18 of DeployNudge.py ## #
        message = f":exclamation: Production Nudge Updated :exclamation:\nThe Nudge configuration has been updated in" \
                  f" Production Jamf Pro. Users in the current *Nudge Pilot* will receive prompts to update " \
                  f"to the latest minor release of macOS."
        debug = False
    if _commit_message.lower().startswith('silent'):
        message += f"\n\nChanges are as follows:\n{_commit_message[6:].lstrip()}"
        notify_slack(message, debug=True)
    else:
        message += f"\n\n{_commit_message}"
        notify_slack(message, debug=debug)


def notify_slack_failure(_run_info, _error):
    """
    Prints debug message and formats error. Calls notify_slack with Message as payload for Slack webhook.

    :param _run_info:
    Dictionary with the run options 'branch', 'instance', 'commit_message', 'commit_sha'
    :param _error:

    :return:
    """
    print(f"Failed to update Jamf Config Profile on {_run_info['instance']}.")
    message = f"Nudge update failure.\nCommit: {_run_info['commit_sha']}\nBranch: {_run_info['branch']}\n"\
        f"Jamf Instance: {_run_info['instance']}\nHTTP Response: {_error}"
    notify_slack(message.replace('"', "'"), debug=True)


def encode_html(_string):
    """
    Encodes string to html, required for passing mobleconfig file contents in payload to the Jamf API.

    :param _string:
    String to encode as HTML for passing as payload
    """
    print("Encoding string in html encoding.")
    return escape(_string)


def update_osx_mobileconfig(_configname, _auth, _mobileconfig_file, _run_info):
    """
    Main function to update mobileconfig in jamf.

    :param _configname:
    Use exact spelling of the configuration profile in Jamf.
    :param _auth:
    jamfAuth object
    :param _mobileconfig_file:
    The file path of the configuration profile to be updated into Jamf
    :param _run_info:
    Dictionary with the run options 'branch', 'instance', 'commit_message', 'commit_sha'
    :return:
    """
    validate_mobileconfig_xml(_mobileconfig_file, _run_info)
    mobileconfig = read_in_mobileconfig(_mobileconfig_file)
    mobileconfig_encoded = encode_html(mobileconfig)
    # Remove the closing parenthesis and comment hash from the following line when ready to deploy
    mobileconfig_payload = build_osx_mobileconfig_payload(mobileconfig_encoded) # , _redeploy='All')
    configname_encoded = encode_configname(_configname)
    response = put_mobileconfig_update(mobileconfig_payload, configname_encoded, _auth)
    print(response)
    if response.status_code == 201:
        notify_slack_success(_run_info)
    else:
        notify_slack_failure(_run_info, response.raw.replace('"', "'"))


def validate_mobileconfig_xml(_xmlfile, _run_info):
    """
    Try to use built in xml library to parse te mobile config file. If an error occurs send debug message to slack and
    raise error.

    :param _xmlfile:
    Path to xml file
    :param _run_info:
    Dictionary consisting of 'branch' and 'instance'
    :return:
    """
    try:
        xml.etree.ElementTree.parse(_xmlfile)
        print(f"XML in {_xmlfile} validated.")
        return True
    except xml.etree.ElementTree.ParseError:
        message = f":warning: XML Parse Error in Nudge Config :warning:\nPush of mobileConfig to " \
                  f"{_run_info['instance']} has failed, please validate before pushing to the {_run_info['branch']} " \
                  f"branch."
        notify_slack(message, True)
        raise jamfAPI_InvalidMobileConfig(f'The {_xmlfile} Mobile Config profile not a valid XML document') from None


class jamfAPI_ResponseError(Exception):
    def __init__(self, message):
        Exception.__init__(self, message)


class jamfAPI_InvalidMobileConfig(xml.etree.ElementTree.ParseError):
    def __init__(self, message):
        xml.etree.ElementTree.ParseError.__init__(self, message)


class jamfAPI_InvalidArguments(TypeError):
    def __init__(self, message):
        Exception.__init__(self, message)


class jamfAuth_InvalidArguments(TypeError):
    def __init__(self, message):
        Exception.__init__(self, message)


class jamfAuth_TypeError(TypeError):
    def __init__(self, message):
        Exception.__init__(self, message)
