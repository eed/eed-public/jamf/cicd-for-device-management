# <font color=red>This guide is currently a work in progress.</font>

# CI/CD for Device Management
## Automatically Pushing a Nudge Configuration to Jamf Pro
### Written and Developed by Samuel Zamvil, Systems Engineer on Stanford University's Enpoint Engineering and Development Team

## CI/CD for System Engineers
The majority of my career as a Systems Engineer has been spent automating, just about everything from installing software to making API calls between multiple systems using serverless code in the cloud. As System Engineers we're always trying to reduce the time it takes to accomplish a task, and a lot of times limiting the human factor and preventing mistakes. So why CI/CD, well it helps to do both and much more. Using CI/CD we can automatically vet for preventable errors and facilitate pushing configuration changes to dev and production environments.

In this example I will be using Git, GitLab, Google Cloud Platform, Python, Slack, and VSCode to autmatically push configuration changes to Jamf via the Jamf API. There is a lot of GitLab specific setup and configurations. However, these controls can be adpted for other platforms like GitHub, AWS, and SAAS applications which are extensible using an API.

## Disclaimers

GCP Permission controls are out of scope for this article but I highly suggest you familiarize yourself with GCP permissions and controlling least privilage for services and users. The setup we will be creating will require additional steps to adhear to best practices.

## Creating Gitlab Project

* Start by creating a GitLab project. This can be done on the web by selecting "New Project" on the Projects page.
* Select "Create Blank Project"
* Name your project and slug, and define the user/group, followed by "Create Project"

## Defining Branches

* Select "Repository/Branches" from the sidebar
* Select "New branch" from the top right
* Name the new branch "prod" and leave "Create from" from our main branch
* Repeate these steps for a branch named "dev"
* From here go to "Settings/Repository" in the side bar
* Expand "Default branch"
* Select "dev" as the default branch and save
    * This seems counter intuitive but this will become apparent later on. It allows us to update our Merge template at the same time we update our configuration, which is eventually used for our Slack notifications.
* Expand "Protected branches"
* Select "prod" as the branch
* Set "Allowed to merge" to "Developers and Maintainers", and "Allowed to push" to "No one"
* Click protect

What we've just done is protect our "prod" branch from direct pushes. This step is very important since our automation will be making changes to different Jamf instances based on which branch we commit to, and we want to create our merge requests within the GitLab interface.

## Configuring Repository

* Select "Settings/General" from the sidebar
* Under "Merge options" uncheck "Enable 'Delete source branch' option by default"
* Select "Save changes"

Since we will be merging from "dev" to "prod" repeatedly we don't want to accidently delete our "dev" branch during a merge

## Setup GCP Mirror Repo

I could reinvent the wheel creating a separate tutorial but I would have trouble replicating the amount of detail Google has already provided. This is why I am linking their "Mirroring Gitlab Repository" tutorial instead.

Start the guide at "Generate static credentials" and make sure to replace gitlab-csr-mirror with your project name.

[Mirroring GitLab repositories to Cloud Source Repositories](https://cloud.google.com/architecture/mirroring-gitlab-repositories-to-cloud-source-repositories)

## Setup API Credentials

* Login to your development Jamf instance.
    * If you are a Jamf Cloud customer and do not have a dev instance I suggest reaching out to your local rep for getting one provided to you.
* Go to "Settings/System Settings/Jamf Pro User Accounts & Groups"
* Select "Add Account"
* Select a username for your account and use the following settings
    * Access Level = Full Access
    * Privilege Set = Custom
    * Access Status = Enabled
* Select a long secure password for this account. I suggest you use a 128 character randomly generated password for all API credentials
* Save the account
* Select "Edit"
* Go to the "Privileges" Tab
* Under "Jamf Pro Server Objects" change the following
    * Find "macOS Configuration Profiles" then check Read and Update
* Repeat these steps in your production Jamf instance

## Create Nudge Configurations

* In Jamf go to "Computers/Configuration Profiles"
* Select "New"
* Name and Save your Configuration Profile
* Repeate steps for both Jamf Instances using the same configuration name

## Generate Slack Webhooks

Again I could generate a tutorial on this that would be inferior to what's already available, so I am linking Slack's tutorial. I have my slack app linked to two individual Slack channels once for debug messages and the other is a public channel. Stop the tutorial once you have your Webhook URLs which look like the following;
>https://hooks.slack.com/services/T00000000/B00000000/XXXXXXXXXXXXXXXXXXXXXXXX

[Sending messages using Incoming Webhooks](https://api.slack.com/messaging/webhooks)

## Cloning your repository with `git`

* In the Project repository page in GitLab select make sure "dev" is the selected branch
* Select clone from the top right
* Pick your "Clone" method by selecting the clipboard button
* Open your preffered terminal application
* Change directories (`cd`) to a preffered location on your disk
* Enter the following at the prompt `git clone <Paste from Clipboard>`
    * If you are on macOS and do not have XCode Command line tools installed you will be prompted to install them
    * It's best to configure `git` at this point as well as setup authentcation in GitLab, the following guide will explain how to set that up
        * [Git on the command line](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git)

## Copying Tutuorial Resources

* Navigate to the resources folder in the [Tutorial Repo](https://code.stanford.edu/eed/eed-public/jamf/cicd-for-device-management/-/tree/main/Resources)
    * Select the Download Icon and "Download this directory"
* Extract the contents of the downloaded archive into your local project folder
* Make sure the folder the ".gitlab/merge_request_templates" folder exists and contains Default.md

## First Commit

* To save our work, within terminal using the project directory as the working directory run the following command `git add .`
    * What this does is it stages all the files we just added to the project directory. Staging allows us to pick which files are saved when we commit them.
* Afterwards run the following command `git commit -m "First Commit"`
    * This has saved our project as a commit using "First Commit" as the commit message

## Editing the project files

The provided files do about 99% of the work. However, they will need to be modified slightly.
* Using an IDE of your choice open the project folder. I use PyCharm and Visual Studio Code on a regular basis. Either one should work for this step.
* In the IDE open "DeployNudge.py"
    * On lines 16 and 18, name your Jamf Instances or leave them as the default "Development" and Production.
    * On line 31, replace "<Your Nudge Configuration Profile Name\>" with the exact name you created earlier in the tutorial
    * On line 32, we have the relitive file path of the demo Nudge mobileconfig file, leave this the same or replace with the relitive path of your own mobileconfig file
        * The file provided in this tutorial is the [example profile](https://github.com/macadmins/nudge/blob/main/Example%20Assets/com.github.macadmins.Nudge.mobileconfig) found on the Nudge github. It's recommended that you either use your own or modify the existing profile following the [Nudge Wiki](https://github.com/macadmins/nudge/wiki) to configure Nudge for your own usecase.
* Still in the IDE open "module/jamfAPI.py"
    * On lines 209 and 211 enter the Slack Webhooks URLs you generated earlier.
        * If you choose to only use one Slack Channel enter the same URL on both lines.
        * Should you choose not to use slack comment out lines 295-298
    * If you modified the values on lines 16 or 18 of "DeployNudge.py", mirror the names on lines 228 and/or 233.
    * As a bonus we can also modify the messages sent to Slack in this same function

## Second Commit

This will be a bit familiar to you after going through this once. Subsequent commits required will not be explained.
* Using the project directory as the working directory in Terminal run `git status`
    * This will show us which files have been modified but not staged
* Now run `git add DeployNudge.py modules/jamfAPI.py`
    * Using the `git add` command we can also specify individual files to stage rather than staging all modified files.
* We can run `git status` once again and show the files staged for commit
* Finally, lets commit the changes with `git commit -m "Modified DeployNudge.py and jamfAPI.py."`
    * You can put any message you like after `-m`. More detail is better than less, you might want to revert to a previous commit and having a good description will keep you from digging into the code to see what has changed.

## First Push

Pushing is the process of uploading changes from our local repository to our GitLab repositiory. This will be recuring process we use to update our Nudge configuration.
* In the same directory run `git push`

## Secret Setup

We'll now want to go ahead and setup a secret in GCP for our build trigger to access our Jamf instances. This is going to secure our API password in a safe environment which we can has a multitude of controls to prevent unauthorized access.
* In the GCP Console navigate to "Security/Secret Manager"
* Select "Create Secret" from the top
* Name your secret for the password to one of the API accounts you created earlier
* Enter the password in the "Secret value" field
* Repeate this for the second API Account

## Configuring the Cloud Build configuration

Cloud Build is configured in YAML or JSON format and I've provided a template we can use for our Build Triggers. There's two ways to configure this, either by having a cloudbuild file in our repository, or to use the inline configurations. Because we want to use the same repository and branches over and over we will be using the inline configuration and modifying it for each trigger.

* Open the ".trigger_mirror_python.cloudbuild.yaml" file in your editor of choice
* With the secret of the password for your Development Jamf Instance open select the "VERSIONS" tab
* To the right of "Versions/Version 1" there is an "Actions" Column
* Click on the 3 dot menu and select "Copy Resource ID"
* Now on line 19 of the ".trigger_mirror_python.cloudbuild.yaml" replace "projects/..." with the resource copied to your clipboard.
* Since we only have one template file we will following the process to copy the resource once again but pasting the resource ID into the inline configurator.

## Setting up our Build Trigger

Now that we have our script and repository the way we want it, lets go ahead and setup our build trigger. We'll want to reference the file ".trigger_mirror_python.cloudbuild.yaml"
* Keep the secret tab open and open another tab in the GCP Console
* Find and select "Cloud Build" from the sidebar
* Navigate to "Triggers"
* Now select "Create trigger"
* In the "Create trigger" page
    * Name your build trigger
    * "Source" 
        * Select the repository we just created
        * Select the "dev" branch
    * "Configuration"
        * We will keep "Cloud Build configuration file (yaml or json)"
        * In "Location" select the "Inline" radio icon
        * Now click "OPEN EDITOR"
        * In the newly opened editor window paste the contents of our ".trigger_mirror_python.cloudbuild.yaml" file followed by "DONE"
    * "Advanced"
        * In "Substitution variables" select "ADD VARIABLE" and enter the following adding the additional 2 variables

        | Variable      | Value |
        | ----------- | ----------- |
        | _URI      | https://<Your Dev Jamf URL\>       |
        | _USERNAME   | <Your API Username\>        |
        | _Message   | $(commit.commit[0].message)        |
* Save the build trigger
* Now create the next trigger for your production instance
    * This time we will modify the Editor configuration to include the Secret Resource ID of the production secret
    * For the _URI substitution variable use the URL of your production server

## Allowing the Cloud Build Service account secret access
<font color=red>Disclaimer</font>
-
This next step does not follow best practices for permissions in GCP. The topic of permissions is out of scope of due to their complex nature. It is highly recommended that you learn how to create a service account to run this build trigger on your own and give it the least amount of permissions to accomplish the recurring task.

We are going to add access to our Secrets to the Cloud Build Service account. More information [Here](https://cloud.google.com/build/docs/cloud-build-service-account)
* Go to "Security/Secret Manager" in the GCP Console sidebar
* Click the checkboxes for both of the secrets you created for your Jamf API passwords
* In the righthand menu select "ADD PRINCIPAL"
* In the "New principals" textbox add your Cloud Build Service Account
    * This follows the standard "[PROJECT_NUMBER\]@cloudbuild.gserviceaccount.com"
        * To find your project number go to the home page by clicking "Google Cloud" in the top left.
        * "Project number" us under the line "You're working in..."

## Testing our configuration
This steps going to be easy if you've been following along.

* Go to your local repository and commit a change
* Push that change to GitLab
* Did you get a Slack message? Probably not, it's time to troubleshoot.

## Troubleshooting the Pipeline 
## <font color=orange>This section is still being finished</font>

Now for the fun part, debugging. It's not often I set something up that's this complex and it works with no errors so we'll go through troubleshooting first then the process. The majority of the errors will show in GCP since our Mirror Repo configuration in GitLab will be static.


## git Troubleshooting
Make sure you're on the "dev" branch and when you push the changes show in GitLab.
* That's it.

## GitLab Troubleshooting
After we've confirmed the "dev" branch was updated in GitLab.
* Go to "Settings/Repository" from the side bar
* Expand the "Mirroring repositories" section
* Take a look at last successful update
    * What was the time of your last push? Was it within 5 minutes? If so you're good
* Once you've confirmed it's good, you shouldn't have to check this again. 

Nows probably a good time to mention. GitLab will only push to a repository every 5 minutes unless you choose "Mirror only protected branchs" which changes the interval to one minute. This can be annoying when troubleshooting but isn't a problem when we use the process in production. 

## GCP Troubleshooting
This section will be divided into parts since there can be a number of different errors.
### Troubleshooting Source Repositories


### Troubleshooting Cloud Build
Here's where we'll see the majority of our errors. Whether it's permissions or something in the code. There's quite a bit of error handling built into the python deployment code you downloaded, so hopefully the errors you see will point you in the right direction.


## Jamf Troubleshooting
There's not much too this either, assuming your API account has the correct permissions and you've provided the right username/password in cloud build. What we can do is check to see that our configuration is getting update.
* Go to "Computers/Configuration Profiles"
* Select your Nudge Configuration
* Click on "History"
* Was the policy modified by your API Account? If not we have an issue somewhere earlier in the pipeline

## The finishing touches
I left a safeguard in to prevent changes to any config profile from being deployed to all devices, instead changes are sent to new devices only by default.

* In the "jamfAPI.py" file go to line 192 and remove the closing parenthesis and comment hash so the line matches the following
    * Make sure the four spaces are included if copying and pasting or the script will error out.

python
```
    mobileconfig_payload = build_osx_mobileconfig_payload(mobileconfig_encoded, _redeploy='All')
```

## The Admin workflow
After all of this hard work is done we get to sit back relax and update our nudge configuration in less than 5 minutes, not including testing of course. Let's assume macOS just receive a minor update, I'll walk you through the steps to get update notification to our users.

* Modify the Nudge Configuration with the target macOS version and required install date
* Modify "Resources/.gitlab/merge_request_templates/Default.md" with the same changes
* Commit and push your repository to GitLab
* TEST!
* Now that you've tested your configuration it's time to merge
    * Note: If you merge within 5 minutes of pushing to dev you will have to wait or push the change manually using the "Mirroring Repositories" section in GitLab "Repository/Settings"
* Select "Merge requests" from the sidebar
* Select "New merge request" from the top right
    * For "Source branch" select "dev"
    * For "Target branch" select "prod"
* In your description you should see the contents of the Default.md file you modified earlier
    * Prepending "silent" to the Description will prevent a slack message from being fired to the notification channel, and instead the message will be sent to the debug channel
* Select "Create merge request"
* You can now either assign tasks to the merge request, like assign an approver or you can complete the merge.
    * Closing the merge request will invalidate the merge request and the production branch will remain unchanged.
* Once the merge is complete you can check your computer or the Jamf console to verify that the change got pushed down.

## Conclusion
Is it all working? Did you really make it this far?

Congrats!!

Questions? Suggestions? Feel free to reach out to me in the Mac Admin Slack.